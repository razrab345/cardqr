<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $fillable = [
        'id',
    ];


    public function getSecretAttribute()
    {
        $key = env('APP_KEY');
        return strtoupper(md5($this->number . $key));
    }

    public function getNumberAttribute()
    {
        return str_pad($this->id, 7, '0', STR_PAD_LEFT);
    }

    public static function check($number, $secret)
    {
        $key = env('APP_KEY');
        $number = str_pad($number, 7, '0', STR_PAD_LEFT);
        $hash = md5($number . $key);
        return $hash === $secret;
    }

}
