<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Card;
use App\Helpers\CardMakerHelper;

class CardController extends Controller
{
    public function store()
    {
        $card = Card::create();
        $number = $card->number;
        $cardCode = $card->number.$card->secret;

        CardMakerHelper::makeImage($cardCode, $number);

        return view('card');
    }

    public function check(Request $request) //если нужно проверить карту
    {
        dd(Card::check($request->number, $request->secret));
    }


}
