<?php

namespace App\Helpers;

use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;
use Storage;
use Intervention\Image\ImageManagerStatic as Image;

class CardMakerHelper
{
    public static function makeImage($cardCode, $number)
    {
        $writer = new PngWriter();

        $qrCode = QrCode::create($cardCode)
            ->setEncoding(new Encoding('UTF-8'))
            ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->setSize(125)
            ->setMargin(10)
            ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->setForegroundColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));

        $result = $writer->write($qrCode);
        Storage::disk('public')->put($number . '.png', $result->getString());

        $QRimage = 'storage/' . $number . '.png';
        $layoutImage = Image::make('img/card.png');
        $layoutImage->insert($QRimage,'top-left', 185, 40);
        $layoutImage->text($number, 260, 195, function($font) {
            $font->file(public_path('fonts/CREDC.ttf'));
            $font->size(20);
            $font->color('#00000');
            $font->align('center');
            $font->valign('top');
        });
        $layoutImage->save('img/card-final.png');

        return $layoutImage;
    }

}
